FROM java:8-jdk-alpine

COPY ./target/interview.assignment-1.0-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch interview.assignment-1.0-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","interview.assignment-1.0-SNAPSHOT.jar"]