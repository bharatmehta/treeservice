package com.bharatmehta.treeservice.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bharatmehta.treeservice.AbstractIntegrationTest;
import com.bharatmehta.treeservice.TestUtils;
import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.repository.TreeDataRepository;
import java.io.IOException;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Integration test for {@link TreeDataController}
 */
@AutoConfigureMockMvc
public class TreeDataControllerIntegrationTest extends AbstractIntegrationTest {

  private static final String PATH = "/tree";

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private TreeDataRepository treeDataRepository;

  @Before
  public void setup() throws IOException {

    List<TreeData> testData = TestUtils.treeData();

    treeDataRepository.deleteAll();

    treeDataRepository.saveAll(testData);
  }

  @Test
  public void shouldReturnNoContentCorrectCoordinate() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(PATH)
        .param("point", "0,0")
        .param("radius", "0"))
        .andExpect(status().isNoContent())
        .andExpect(content().bytes(new byte[0]));
  }

  @Test
  public void shouldReturnNoContentWithNoRadius() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(PATH)
        .param("point", "0,0"))
        .andExpect(status().isNoContent())
        .andExpect(content().bytes(new byte[0]));
  }

  @Test
  public void shouldReturnOk() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(PATH)
        .param("point", "1027431.148, 202756.7687")
        .param("radius", "0"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(content().json("{\"red maple\":1}"));
  }


  @Test
  public void shouldReturnBadRequestWithMissingValuesInPoint() throws Exception {
    String[][] requestParameters = {
        {"0.0", "0.0"},
        {"0,0", "Test"},
        {"Test,0", "0"},
        {"0,Test", "0"},
        {"Test,Test", "0"},
        {"Test,Test", "TEST"},
        {"0#0", "0"},
        {"0,0,0", "0"},
        {"", "0.00"}};

    for (int i = 0; i < requestParameters.length; i++) {
      mockMvc.perform(MockMvcRequestBuilders.get(PATH)
          .param("point", requestParameters[i][0])
          .param("radius", requestParameters[i][1]))
          .andExpect(status().isBadRequest())
          .andExpect(content().bytes(new byte[0]));
    }

    mockMvc.perform(MockMvcRequestBuilders.get(PATH)
        .param("x", "190")
        .param("y", "190")
        .param("radius", "0"))
        .andExpect(status().isBadRequest())
        .andExpect(content().bytes(new byte[0]));
  }

  @Test
  public void shouldReturnNotFoundWithNegativeRadius() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(PATH)
        .param("point", "0,0")
        .param("radius", "-1.0"))
        .andExpect(status().isNotFound())
        .andExpect(content().bytes(new byte[0]));
    ;
  }

}
