package com.bharatmehta.treeservice.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bharatmehta.treeservice.model.Point;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bharatmehta.treeservice.service.TreeDataService;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Unit test for {@link TreeDataController}
 */
@RunWith(MockitoJUnitRunner.class)
public class TreeDataControllerTest {

  private static final String PATH = "/tree";

  private MockMvc mockMvc;

  private ObjectMapper mapper;

  @Mock
  private TreeDataService treeDataService;

  @InjectMocks
  private TreeDataController controllerUnderTest;

  private Point point;

  private double radius;


  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();

    ReflectionTestUtils.setField(controllerUnderTest, "treeDataService", treeDataService);

    mapper = new ObjectMapper();

    point = new Point(90.00, 90.00);

    radius = 500.00;
  }


  @Test
  public void shouldReturnNoContent() throws Exception {

    when(treeDataService.countWithinCircle(point, radius))
        .thenReturn(Optional.empty());

    mockMvc.perform(get(PATH)
        .param("point", "90.00,90.00")
        .param("radius", "500.00"))
        .andExpect(status().isNoContent())
        .andExpect(content().bytes(new byte[0]));

    verify(treeDataService).countWithinCircle(point, radius);
  }

  @Test
  public void shouldReturnOk() throws Exception {
    Map<String, Long> treeCount = new HashMap<>();
    treeCount.put("Alpine", 90L);
    treeCount.put("Oak", 0L);

    when(treeDataService.countWithinCircle(point, radius))
        .thenReturn(Optional.of(treeCount));

    mockMvc.perform(get(PATH)
        .param("point", "90.00,90.00")
        .param("radius", "500.00"))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isOk())
        .andExpect(content().json(mapper.writeValueAsString(treeCount)));

    verify(treeDataService).countWithinCircle(point, radius);
  }

}
