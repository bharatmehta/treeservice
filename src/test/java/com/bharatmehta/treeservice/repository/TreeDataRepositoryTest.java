package com.bharatmehta.treeservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.bharatmehta.treeservice.TestUtils;
import com.bharatmehta.treeservice.AbstractIntegrationTest;
import com.bharatmehta.treeservice.model.TreeData;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TreeDataRepositoryTest extends AbstractIntegrationTest {

  @Autowired
  private TreeDataRepository repository;

  private List<TreeData> treeData;

  @Before
  public void setup() throws IOException {
    treeData = TestUtils.treeData();
  }


  @Test
  public void shouldPassFindWithin() {
    repository.saveAll(treeData);

    List<TreeData> treeData = repository.findWithin(1027431.148, 202756.7687, 1000);

    assertThat(treeData.size()).isEqualTo(6);

    Map<String, Long> treeCount = treeData.stream()
        .collect(Collectors.groupingBy(TreeData::getSpc_common, Collectors.counting()));

    assertThat(treeCount.get("red maple")).isEqualTo(1L);
    assertThat(treeCount.get("honeylocust")).isEqualTo(3L);
    assertThat(treeCount.get("tulip-poplar")).isEqualTo(1L);
    assertThat(treeCount.get("Callery pear")).isEqualTo(1L);
  }

  @Test
  public void shouldReturnEmptyList() {
    repository.saveAll(treeData);

    assertThat(repository.findWithin(0.00, 0.00, 998.99934383).isEmpty()).isTrue();
  }


}
