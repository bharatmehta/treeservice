package com.bharatmehta.treeservice;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public abstract class AbstractIntegrationTest {

  @ClassRule
  public static DockerComposeRule docker = DockerComposeRule.builder()
      .file("src/test/resources/db/postgres.yml")
      .waitingForService("db", HealthChecks.toHaveAllPortsOpen())
      .build();

}
