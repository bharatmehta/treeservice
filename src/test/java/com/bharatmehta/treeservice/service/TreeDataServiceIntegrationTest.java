package com.bharatmehta.treeservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.bharatmehta.treeservice.AbstractIntegrationTest;
import com.bharatmehta.treeservice.TestUtils;
import com.bharatmehta.treeservice.model.Point;
import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.repository.TreeDataRepository;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Integration tests for {@link TreeDataService}
 */
public class TreeDataServiceIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private TreeDataService treeDataService;

  @Autowired
  private TreeDataRepository treeDataRepository;

  private List<TreeData> treeData;

  @Before
  public void setup() throws IOException {
    treeData = TestUtils.treeData();

    treeDataRepository.deleteAll();
    treeDataRepository.saveAll(treeData);
  }


  @Test
  public void shouldPassWithNoCounts() {
    Optional<Map<String, Long>> treeCount = treeDataService
        .countWithinCircle(new Point(0.0, 0.0), 0);
    assertThat(treeCount.isPresent()).isFalse();
  }

  @Test
  public void shouldPassRedMaple() {
    Optional<Map<String, Long>> treeCount = treeDataService
        .countWithinCircle(new Point(1027431.148, 202756.7687), 00.00);
    assertThat(treeCount.isPresent()).isTrue();
    assertThat(treeCount.get().size()).isEqualTo(1);
    assertThat(treeCount.get().get("red maple")).isEqualTo(1L);
  }


  @Test
  public void shouldPassWithTreeCount() {
    Optional<Map<String, Long>> treeCount = treeDataService.countWithinCircle(
        new Point(1027431.148, 202756.7687), 304.8);

    assertThat(treeCount.get().size()).isEqualTo(4);
    assertThat(treeCount.get().get("red maple")).isEqualTo(1L);
    assertThat(treeCount.get().get("honeylocust")).isEqualTo(3L);
    assertThat(treeCount.get().get("tulip-poplar")).isEqualTo(1L);
    assertThat(treeCount.get().get("Callery pear")).isEqualTo(1L);
  }

}
