package com.bharatmehta.treeservice.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.support.TreeOpenDataApiMonitor;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class TreeOpenDataSchedulerTest {

  @Mock
  private TreeDataService treeDataService;

  @Mock
  private TreeOpenDataClient client;

  @Mock
  private TreeOpenDataApiMonitor treeOpenDataApiMonitor;

  @InjectMocks
  private TreeOpenDataClientScheduler scheduler;

  private List<TreeData> treeData;


  @Before
  public void setup() throws IOException {
    ReflectionTestUtils.setField(scheduler, "enabled", true);
    String dummyApiResponse = new String(
        Files.readAllBytes(Paths.get("src/test/resources/opendataapi/response.json")), "UTF-8");

    ObjectMapper objectMapper = new ObjectMapper();

    treeData = Arrays.asList(objectMapper.readValue(dummyApiResponse, TreeData[].class));
  }


  @Test
  public void shouldNotDoAnything() {
    ReflectionTestUtils.setField(scheduler, "enabled", false);

    scheduler.fetchAndSaveData();

    verify(client, never()).findAll();
    verify(treeDataService, never()).replaceDataInDB(any());
    verify(treeOpenDataApiMonitor, never()).countFailedApiHit();
    verify(treeOpenDataApiMonitor, never()).countApiHit();
  }


  @Test
  public void shouldSaveDataInDB() {
    when(client.findAll())
        .thenReturn(treeData);

    scheduler.fetchAndSaveData();

    verify(treeDataService).replaceDataInDB(treeData);
    verify(treeOpenDataApiMonitor).countApiHit();
    verify(treeOpenDataApiMonitor, never()).countFailedApiHit();
    verify(treeOpenDataApiMonitor, never()).countNoData();
  }

  @Test
  public void shouldNotSaveDataInDB() {
    when(client.findAll())
        .thenReturn(null);

    scheduler.fetchAndSaveData();

    verify(treeDataService, never()).replaceDataInDB(null);
    verify(treeOpenDataApiMonitor).countApiHit();
    verify(treeOpenDataApiMonitor, never()).countFailedApiHit();
    verify(treeOpenDataApiMonitor).countNoData();
  }

  @Test
  public void shouldNotSaveDataInDBWhenEmptyResponse() {
    when(client.findAll())
        .thenReturn(Collections.emptyList());

    scheduler.fetchAndSaveData();

    verify(treeDataService, never()).replaceDataInDB(Collections.emptyList());
    verify(treeOpenDataApiMonitor).countApiHit();
    verify(treeOpenDataApiMonitor, never()).countFailedApiHit();
    verify(treeOpenDataApiMonitor).countNoData();
  }

  @Test
  public void shouldNotSaveDataInDBWhenException() {
    when(client.findAll())
        .thenThrow(RuntimeException.class);

    scheduler.fetchAndSaveData();

    verify(treeDataService, never()).replaceDataInDB(any());
    verify(treeOpenDataApiMonitor).countApiHit();
    verify(treeOpenDataApiMonitor).countFailedApiHit();
    verify(treeOpenDataApiMonitor, never()).countNoData();
  }
}
