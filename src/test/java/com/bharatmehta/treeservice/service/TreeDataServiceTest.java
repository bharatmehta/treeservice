package com.bharatmehta.treeservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bharatmehta.treeservice.TestUtils;
import com.bharatmehta.treeservice.model.Point;
import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.repository.TreeDataRepository;
import com.bharatmehta.treeservice.support.TreeServiceMonitor;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Unit tests for {@link TreeDataService}
 */
@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class TreeDataServiceTest {

  @Mock
  private TreeServiceMonitor treeServiceMonitor;

  @Mock
  TreeDataRepository treeDataRepository;

  @InjectMocks
  TreeDataService treeDataService;

  List<TreeData> treeData;


  @Before
  public void setup() throws IOException {
    treeData = TestUtils.treeData();
  }

  @Test
  public void shouldPassWithOneTreeCount() {
    final double x = 1027431.148;
    final double y = 202756.7687;
    final double radiusMeters = 1;
    final double radiusFeet = 3.2808;

    when(treeDataRepository.findWithin(x, y, radiusFeet))
        .thenReturn(Arrays.asList(treeData.get(0)));

    Optional<Map<String, Long>> treeCount = treeDataService.countWithinCircle(new Point(x, y),
        radiusMeters);

    verify(treeDataRepository).findWithin(x, y, radiusFeet);

    verify(treeServiceMonitor).countRequest();

    assertThat(treeCount.isPresent()).isTrue();

    assertThat(treeCount.get()).hasSize(1);

    assertThat(treeCount.get().get("red maple")).isEqualTo(1L);
  }

  @Test
  public void shouldPassRepositoryReturnsNoResult() {
    final double x = 1027431.148;
    final double y = 202756.7687;

    when(treeDataRepository.findWithin(x, y, 0.0))
        .thenReturn(Collections.emptyList());

    Optional<Map<String, Long>> treeCount = treeDataService
        .countWithinCircle(new Point(x, y), 0.00);

    verify(treeDataRepository).findWithin(x, y, 0.0);

    verify(treeServiceMonitor).countRequest();

    assertThat(treeCount.isPresent()).isFalse();
  }


  @Test
  public void shouldFailWithNPE() {
    assertThatThrownBy(() -> treeDataService.countWithinCircle(null, 0.0))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void shouldFailWithIllegalArgumentException() {
    assertThatThrownBy(
        () -> treeDataService.countWithinCircle(new Point(1008615.374, 224096.274), -1.0))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void shouldNotCallRepository() {

    assertThatThrownBy(() -> treeDataService.replaceDataInDB(null))
        .isInstanceOf(NullPointerException.class);

    treeDataService.replaceDataInDB(Collections.emptyList());

    verify(treeDataRepository, never()).saveAll(any());
    verify(treeDataRepository, never()).deleteAll();
  }

}
