package com.bharatmehta.treeservice.service;


import static org.assertj.core.api.Assertions.assertThat;

import com.bharatmehta.treeservice.AbstractIntegrationTest;
import com.bharatmehta.treeservice.model.TreeData;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Integration tests for {@link TreeOpenDataClient}
 */
public class TreeOpenDataClientIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private TreeOpenDataClient treeOpenDataClient;


  @Test
  public void shouldGetTreeData() {
    List<TreeData> treeData = treeOpenDataClient.findAll();
    assertThat(treeData.isEmpty()).isFalse();
    treeData.stream().forEach(
        tree -> {
          assertThat(tree.getTree_id()).isNotNull();
          assertThat(tree.getX_sp()).isNotNull();
          assertThat(tree.getY_sp()).isNotNull();
          assertThat(tree.getState()).isNotBlank();
        }
    );
  }

}
