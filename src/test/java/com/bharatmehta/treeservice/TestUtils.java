package com.bharatmehta.treeservice;

import static java.nio.file.Files.readAllBytes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.bharatmehta.treeservice.model.TreeData;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Test Utility
 */
public final class TestUtils {

  public static List<TreeData> treeData() throws IOException {
    String dummyApiResponse = new String(
        readAllBytes(Paths.get("src/test/resources/opendataapi/response.json")), "UTF-8");

    ObjectMapper objectMapper = new ObjectMapper();

    return Arrays.asList(objectMapper.readValue(dummyApiResponse, TreeData[].class));
  }

  private TestUtils() {
  }


}

