package com.bharatmehta.treeservice.configuration;

import com.bharatmehta.treeservice.service.TreeOpenDataClient;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class AppConfiguration {

  @Bean
  public Docket newApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .useDefaultResponseMessages(false)
        .apiInfo(apiInfo())
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.holidu.interview.treeservice.controller"))
        .build();

  }

  @Bean
  public TreeOpenDataClient treeOpenDataClient(@Value("${opendata.url}") final String url) {
    return Feign.builder()
        .client(new OkHttpClient())
        .encoder(new JacksonEncoder())
        .decoder(new JacksonDecoder())
        .logger(new Slf4jLogger(TreeOpenDataClient.class))
        .logLevel(Logger.Level.FULL)
        .target(TreeOpenDataClient.class, url);
  }


  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("TreeCounter in NY")
        .description("Interview Assignment")
        .build();
  }
}
