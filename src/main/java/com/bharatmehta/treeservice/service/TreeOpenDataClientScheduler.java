package com.bharatmehta.treeservice.service;

import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.support.TreeOpenDataApiMonitor;
import java.util.List;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduler to fetch {@link TreeData} using {@link TreeOpenDataClient} and save in database
 */
@Component
public class TreeOpenDataClientScheduler {

  private static final Logger log = LoggerFactory.getLogger(TreeOpenDataClientScheduler.class);

  private final TreeOpenDataClient treeOpenDataClient;

  private final TreeDataService treeDataService;

  private TreeOpenDataApiMonitor monitor;

  @Value("${opendata.scheduler.enabled}")
  private boolean enabled;

  public TreeOpenDataClientScheduler(@NonNull TreeOpenDataClient treeOpenDataClient,
      @NonNull TreeDataService treeDataService , @NonNull TreeOpenDataApiMonitor monitor) {
    this.treeOpenDataClient = treeOpenDataClient;
    this.treeDataService = treeDataService;
    this.monitor = monitor;
  }

  @Scheduled(fixedRateString = "${opendata.scheduler.fixedRate.milliseconds}")
  public void fetchAndSaveData() {

    if (enabled) {
      log.info("Calling {} to fetch tree data", treeOpenDataClient.getClass().getSimpleName());

      try {
        monitor.countApiHit();

        List<TreeData> apiData = treeOpenDataClient.findAll();
        if (apiData == null || apiData.isEmpty()) {
          monitor.countNoData();
        } else {
          treeDataService.replaceDataInDB(apiData);
        }


      } catch (Exception e) {
        monitor.countFailedApiHit();
        log.error("Problem occurred", e);
      }
    }
  }
}
