package com.bharatmehta.treeservice.service;

import com.bharatmehta.treeservice.model.Point;
import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.repository.TreeDataRepository;
import com.bharatmehta.treeservice.support.TreeServiceMonitor;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Service class for counting {@link TreeData} with a search area
 */
@Service
@AllArgsConstructor
public class TreeDataService {

  private static final Logger logger = LoggerFactory.getLogger(TreeDataService.class);

  /**
   * Monitor
   */
  private final TreeServiceMonitor monitor;

  /**
   * Repository
   */
  private final TreeDataRepository repository;


  /**
   * Counts and returns the names of Trees in search Area.
   *
   * @param origin Cartesian Point location of origin/start of search
   * @param radiusMetres Radius in meters
   * @return Optional
   * @throws NullPointerException if <code>origin</code> is null
   * @throws IllegalArgumentException if <code>radiusMeters</code> is negative
   */
  @Transactional(readOnly = true)
  public Optional<Map<String, Long>> countWithinCircle(@NonNull final Point origin,
      final double radiusMetres) {

    Assert.isTrue(radiusMetres >= 0, "radiusMeters can't be negative");

    monitor.countRequest();

    double feet = meterToFeet(radiusMetres);

    Map<String, Long> treeCount = repository.findWithin(origin.getX(), origin.getY(), feet).stream()
        .filter(tree -> StringUtils.hasText(tree.getSpc_common()))
        .collect(Collectors.groupingBy(TreeData::getSpc_common, Collectors.counting()));

    logger.info("{} trees species found from {},{} within radius of {} meters or {} feet",
        treeCount.size(),
        origin.getX(),
        origin.getY(),
        radiusMetres,
        feet);

    return treeCount.isEmpty() ? Optional.empty() : Optional.of(treeCount);
  }

  /**
   * Deletes old data and inserts new
   * @param treeData List of {@link TreeData}
   * @throws NullPointerException  if <code>treeData</code> is null
   */
  @Transactional
  public void replaceDataInDB(@NonNull List<TreeData> treeData) {
    if (!treeData.isEmpty()) {
      repository.deleteAll();
      repository.saveAll(treeData);
    }
  }

  /**
   * Converts meters to feet and rounds upto 4 decimal places
   * @param meter
   * @return
   */
  private double meterToFeet(final double meter) {
    return new BigDecimal(meter / 0.3048)
        .setScale(4, RoundingMode.HALF_UP)
        .doubleValue();
  }

}
