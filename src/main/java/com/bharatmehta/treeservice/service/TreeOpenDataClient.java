package com.bharatmehta.treeservice.service;

import com.bharatmehta.treeservice.model.TreeData;
import feign.Headers;
import feign.RequestLine;
import java.util.List;

/**
 * Feign based client for access open data API
 */
public interface TreeOpenDataClient {

  @RequestLine("GET")
  @Headers("Accept: application/json; charset=UTF-8")
  List<TreeData> findAll();

}
