package com.bharatmehta.treeservice.controller;

import com.bharatmehta.treeservice.model.Point;
import com.bharatmehta.treeservice.service.TreeDataService;
import io.swagger.annotations.ApiParam;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tree")
@RequiredArgsConstructor
public class TreeDataController implements TreeDataApi {

  private static final Logger logger = LoggerFactory.getLogger(TreeDataController.class);

  private final TreeDataService treeDataService;


  @Override
  public ResponseEntity<Map<String, Long>> countTrees(
      @ApiParam(value = "A Cartesian Point along the x & y plane.Enter as x,y", required = true)
      @RequestParam(value = "point") final String point,
      @ApiParam(value = "A Search Radius in meters", required = true)
      @RequestParam(value = "radius", required = false, defaultValue = "0.00") final double radius) {

    final Point origin = convertToPoint(point);

    if (origin != null && radius >= 0.00) {
      Optional<Map<String, Long>> result = treeDataService.countWithinCircle(origin, radius);

      if (result.isPresent()) {
        return ResponseEntity.ok(result.get());
      } else {
        return ResponseEntity.noContent().build();
      }
    }
    return ResponseEntity.notFound().build();
  }

  private Point convertToPoint(final String pointAsString) {
    if (StringUtils.hasText(pointAsString)) {
      String[] values = pointAsString.split(",");
      if (values.length == 2) {
        try {
          final double x = Double.parseDouble(values[0].trim());
          final double y = Double.parseDouble(values[1].trim());

          return new Point(x, y);
        } catch (NumberFormatException e) {
          logger.debug("Exception while parsing parameter: {}", pointAsString, e);
          throw e;
        }
      }
    }
    throw new IllegalArgumentException("Invalid Argument" + pointAsString);
  }


}
