package com.bharatmehta.treeservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Exception Handling for {@link TreeDataController}
 */
@RestControllerAdvice(basePackageClasses = {TreeDataController.class})
public class TreeControllerAdvice {

  @ExceptionHandler(value = {
      HttpMessageNotReadableException.class,
      IllegalArgumentException.class,
      NumberFormatException.class,
      MissingServletRequestParameterException.class,
      MethodArgumentTypeMismatchException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public void  handleHttpMessageNotReadableException(final Exception e) {
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<Void> handleException(final Exception e) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }
}
