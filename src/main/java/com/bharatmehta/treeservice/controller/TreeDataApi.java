package com.bharatmehta.treeservice.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public interface TreeDataApi {

  @ApiResponses({
      @ApiResponse(code = 200, message = "Search Result is found"),
      @ApiResponse(code = 204, message = "Search performed but found no result"),
      @ApiResponse(code = 400, message = "Bad Request"),
      @ApiResponse(code = 404, message = "Not Found"),
      @ApiResponse(code = 500, message = "Unexpected Error")
  })
  @ApiOperation(value = "Counts and lists the names of trees within a search radius")
  @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  ResponseEntity<Map<String, Long>> countTrees(final String point, final double radius);
}
