package com.bharatmehta.treeservice.support;

import com.bharatmehta.treeservice.model.TreeData;
import com.bharatmehta.treeservice.service.TreeOpenDataClient;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * HealthCheck for {@link TreeOpenDataClient}
 */
@Component
public class TreeOpenDataClientHealthCheck implements HealthIndicator {

  @Autowired
  private TreeOpenDataClient treeOpenDataClient;

  @Override
  public Health health() {
    return isDataFetched() ? Health.up().build() : Health.down().build();
  }

  private boolean isDataFetched(){
    List<TreeData> treeData = treeOpenDataClient.findAll();
    return treeData != null && !treeData.isEmpty();
  }
}
