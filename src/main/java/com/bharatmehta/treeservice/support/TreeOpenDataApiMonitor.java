package com.bharatmehta.treeservice.support;


import com.bharatmehta.treeservice.service.TreeOpenDataClient;
import com.bharatmehta.treeservice.service.TreeOpenDataClientScheduler;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

/**
 * Counter for {@link TreeOpenDataClient}
 * in {@link TreeOpenDataClientScheduler}
 */
@Component
public class TreeOpenDataApiMonitor {

  /**
   * Counter for number of requests to open data api
   */
  private final Counter apiHitsCounter;

  /**
   * Counter for number of failed request to open data api
   */
  private final Counter apiHitsFailedCounter;

  /**
   * Counter for number of requests for which no data received
   */
  private final Counter apiHitsNoDataCounter;

  public TreeOpenDataApiMonitor(MeterRegistry meterRegistry) {
    apiHitsCounter = Counter.builder("opendata.api.hits.counter")
        .description("Requests to OpenData API")
        .register(meterRegistry);

    apiHitsFailedCounter = Counter.builder("opendata.api.failed.hits.counter")
        .description("Failed Requests to OpenData API")
        .register(meterRegistry);

    apiHitsNoDataCounter = Counter.builder("opendata.api.nodata.hits.counter")
        .description("No data from  OpenData API")
        .register(meterRegistry);
  }

  public void countApiHit(){
    apiHitsCounter.increment();;
  }

  public void countFailedApiHit(){
    apiHitsFailedCounter.increment();;
  }


  public void countNoData(){
    apiHitsNoDataCounter.increment();;
  }
}
