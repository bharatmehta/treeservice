package com.bharatmehta.treeservice.support;


import com.bharatmehta.treeservice.service.TreeDataService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

/**
 * Monitor for {@link TreeDataService}
 */
@Component
public class TreeServiceMonitor {

  /**
   * Counter for request received
   */
  private final Counter requestCounter;


  public TreeServiceMonitor(MeterRegistry meterRegistry) {
    requestCounter = Counter.builder("treesearch.request.counter")
        .description("Requests to TreeDataService")
        .register(meterRegistry);

  }

  public void countRequest() {
    requestCounter.increment();
  }

}
