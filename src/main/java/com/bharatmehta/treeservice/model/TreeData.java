package com.bharatmehta.treeservice.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "treedata")
public class TreeData {

  @Id
  private Long tree_id;

  private String address;
  private Long bbl;
  private Long bin;
  private Long block_id;
  private Long boro_ct;
  private Long borocode;
  private String boroname;
  private String brch_light;
  private String brch_other;
  private String brch_shoe;
  private Integer cb_num;
  private Integer census_tract;
  private Integer cncldist;
  private Integer council_district;
  private Date created_at;
  private String curb_loc;
  private String guards;
  private String health;
  private Double latitude;
  private Double longitude;
  private String nta;
  private String nta_name;
  private String problems;
  private String root_grate;
  private String root_other;
  private String root_stone;
  private String sidewalk;
  /**
   * Common name of the Tree
   */
  private String spc_common;
  private String spc_latin;
  private Integer st_assem;
  private Integer st_senate;
  private String state;
  private String status;
  private String steward;
  private Double stump_diam;
  private Integer tree_dbh;
  private String trnk_light;
  private String trnk_other;
  private String trunk_wire;
  private String user_type;
  /**
   * Tree's x coordinate
   */
  private Double x_sp;
  /**
   * Tree's y coordinate
   */
  private Double y_sp;
  private String zip_city;
  private Integer zipcode;
}