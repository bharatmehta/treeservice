package com.bharatmehta.treeservice.repository;

import com.bharatmehta.treeservice.model.TreeData;
import java.math.BigInteger;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for TreeData
 */
@Repository
public interface TreeDataRepository extends JpaRepository<TreeData, BigInteger> {

  /**
   * Returns TreeData within a circular radius. Please refer
   * @see <a href="https://www.postgresql.org/docs/current/functions-geometry.html">
   *   https://www.postgresql.org/docs/current/functions-geometry.html</a>
   * @param x X co oridinate
   * @param y Y co ordinate
   * @param radius Radius in feet
   * @return List
   */
  @Query(value = "Select * from public.treedata where point (x_sp,y_sp) <@ circle(point(?1, ?2),?3) ",
      nativeQuery = true)
  List<TreeData> findWithin(double x, double y, double radius);

}
