Purpose

Create a search application where you expose an endpoint for a client to search based on a certain radius for tree related data.

- You can find Street tree data from the TreesCount 2015 here => https://data.cityofnewyork.us/Environment/2015-Street-Tree-Census-Tree-Data/uvpi-gqnh
- The direct call to api is `https://data.cityofnewyork.us/resource/nwxe-4ae8.json`
- You have to expose and API endpoint accepting two parameters 
    1. A Cartesian Point specifying a center point along the x & y plane
    2. A search radius in meters

Output
 - You have to retrieve the count of "common name" (please see in the documentation on the same page above which field to refer to) for all 
 the species of trees in that search radius
 - Expected outcome from the api
```json
{
    "red maple": 30,
    "American linden": 1,
    "London planetree": 3
}
```

Solution

Assumptions

- Distance between (x1,y1) and (x2,y2) is equal to
```java
   Math.sqrt((x1 - x2) * (x1 - x2)+ (y1 - y2) * (y1 - y2));
```
- API is called with GET on :
```
http://localhost:8080/tree?point=90.00,90.00&radius=0.00
```

Tools used

- Java8

- Spring Boot
  - Rapid Application Development with Spring

- Feign Http Client
  - Developer friendly easy to use Http Client 

- Junit4

- Mockito 2

- Lombok

- Postgres
  - Datastore

- Docker Compose
  - For integration testing

- Spring Scheduler
  - For importing TreeData from Open Data Api at regular intervals. For demo it runs after every 5 seconds. Normally the period will be more.
  See src/resources/application.yml for more details

- Spring Actuator
  - [HealthCheck](http://localhost:8080/actuator/health)
  - [Build Info](http://localhost:8080/actuator/info)
  - [Environment](http://localhost:8080/actuator/env)
  - [Metrics Info](http://localhost:8080/actuator/metrics)
  - [Prometheus Monitoring](http://localhost:8080/actuator/prometheus)
      * `treesearch.request.counter` Counter for requests received
      * `opendata.api.hits.counter` Counter for requests to Open Data Api
      * `opendata.api.failed.hits.counter`Counter for request which failed at Open Data Api
      * `opendata.api.nodata.hits.counter` Counter for request when no data was received from Open Data API
             
- Swagger
    - [For API documentation](http://localhost:8080/swagger-ui.html)
     

How to Run


- Build
```
mvn clean install
```
  - Please ensure that port 5432 is not being use. Integration tests with start Postgres container using src/test/resources/db/postgres.yml     

- Running the application

  - Start the Postgres (Port 5432 must be free)
  ```
  docker-compose -f postgres.yml up
  ```
  - Start the application
  ```
  java -jar target/treeservice-1.0-SNAPSHOT.jar
  ``` 
  [Click Here]( http://localhost:8080/swagger-ui.html)
  

